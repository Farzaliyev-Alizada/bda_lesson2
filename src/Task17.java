import java.util.Scanner;

/*
17) 2 soz daxil edilir uzunlugu boyuk olan String geriye return edilir.
 */
public class Task17 {
    public static void main(String[] args) {
        String firstWord;
        Scanner input1 = new Scanner(System.in);
        System.out.println("Please enter first word: ");
        firstWord=input1.nextLine();
        String secondWord;
        Scanner input2 = new Scanner(System.in);
        System.out.println("Please enter second word: ");
        secondWord=input2.nextLine();
        String result = (firstWord.length()>secondWord.length())?firstWord:secondWord;
        System.out.println("The longest word is "+ result);
    }
}
