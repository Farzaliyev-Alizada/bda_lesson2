import java.util.Scanner;

/*
18).Ele bir method yazin ki: HeLLo WorlD-> chevrilsin olsun hEllO wORLd.
 */
public class Task18 {
    public static void main(String[] args) {
        String str;
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a new word which involves both lowercase and uppercase");
        str = input.nextLine();
        String result = "";
        for (int i = 0; i < str.length(); i++) {
            result += (Character.isUpperCase(str.charAt(i)))?str.toLowerCase().charAt(i):str.toUpperCase().charAt(i);
        }
        System.out.println(result);
    }
}
