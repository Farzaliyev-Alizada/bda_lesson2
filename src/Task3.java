/*
3)String t = “             1,e 6,e 0,e 9,e           ”; verilmish setirdeki her bir ededin kvadratlarini chap edin.
*/

public class Task3 {
    public static void main(String[] args) {
        String t = "             1,e 6,e 0,e 9,e           ";
        t= t.replace(" ", "").replace(",", "").replace("e", "");


        for (int i =0;i<t.length();i++){
            int num = Integer.parseInt(String.valueOf(t.charAt(i)));
            int SquareNum = Square(num);
            System.out.println(SquareNum);
        }

    }
    public static int Square (int num) {
        return num * num;
    }


}
