import java.util.Scanner;

/*
12).iki int deyerinin sonuncu elementlerinin cemini tapin.
 */
public class Task12 {
    public static void main(String[] args) {
        String str1;
        Scanner num1 = new Scanner(System.in);
        System.out.println("Enter first number");
        str1= num1.nextLine();
        String str2;
        Scanner num2 = new Scanner(System.in);
        System.out.println("Enter second number");
        str2= num2.nextLine();

        str1=str1.substring(str1.length()-1);
        str2=str2.substring(str2.length()-1);

        Integer number1 = Integer.valueOf(str1);
        Integer number2 = Integer.valueOf(str2);

        int result = number1+number2;

        System.out.println("the sum of the last elements of two int values is : "+result);


    }
}
