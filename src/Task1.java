import java.util.Scanner;

/*
1)Write a Java program to get the character at the given index within the String.*/
public class Task1 {
    public static void main(String[] args) {
    String a;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter any word you want: ");
        a= input.next();
        Scanner digit = new Scanner(System.in);
        System.out.println("Enter chosen index:");
        int num = digit.nextInt();
        System.out.println("The index number " + num + " is " + a.charAt(num));

    }
}
